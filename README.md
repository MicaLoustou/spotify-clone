# Spotify Clone 🎶

---

I made a static and responsive copy of the Spotify home page with only html5 and css3, inspecting the official Spotify page.

## Watch the project on Netlify 👀

---

### [Netlify: Spotify Clone](https://modest-keller-c3b56b.netlify.app/).

## What does the project look like? 🎨

---

1. Index.
   ![index](https://bytebucket.org/MicaLoustou/spotify-clone/raw/01812f4c97a1e03f9132a45cfaa7113981d68b96/index.png)

## Tools 🔧

---

For build this proyect i used:

- HTML 5.
- CSS 3.

## To run this proyect 🏃‍♂️

---

1. Clone this repository.
2. Double click on the index.html file.

---

⌨️ with ❤️ by [Mica Loustou](https://bitbucket.org/MicaLoustou/) 😊
